# cdn

A personal cdn on Gitlab.

## Quick Start

1. Fork this repository.
2. Add A pipeline Trigger: goto Project Settings > CI/CD > Pipeline triggers > Add trigger, and copy the trigger token
3. Add A WebHook: goto Project Settings > Webhooks, fill the URL field with `https://gitlab.com/api/v4/projects/<PROJECT>/ref/<REF_NAME>/trigger/pipeline?token=<TOKEN>`, replace `<PROJECT>` with repo id, and `<REF_NAME>` with brach or tag, `<TOKEN>` with the trigger token above, and check the Issue events option on Trigger section, then click "Add webhook".
4. [New A Issue](../../issues) on the repo above: write down the urls to be used with the cdn in the "description" field, each url as a single line.
5. Replace jsDelivr with cdn: replace `https://cdn.jsdelivr.com/npm/xxx.js` with `/cdn/-/xxx.js`
