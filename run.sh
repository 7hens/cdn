#!/bin/bash

project_dir="$(dirname $(realpath "${BASH_SOURCE[0]}"))"
public_dir="$project_dir/public"

main() {
    local repo_url=$(git remote get-url origin)
    local project_url=$(git_api_repo "$repo_url")
    local issues_url="$project_url/issues?order_by=updated_at&per_page=1"
    local description=$(curl -s "$issues_url" | json_get description)
    description=${description//\\n/ }
    mkdir -p "$public_dir"
    for res_url in ${description[@]}; do
        download_res $res_url
    done

    cp *.html *.md "$public_dir"
    ln_s "$public_dir/-" "$project_dir/-"
    # make it compatible for old version
    for f in "$public_dir/-"/*; do
        ln_s "$f" "$public_dir/$(basename $f)"
    done
    ls "$public_dir"
}

git_url_domain() {
    local repo_url="$1"
    local domain=$(printf "$repo_url" | sed 's#\(.*\.com\)[:/].*#\1#g')
    local protocol
    if [[ "$domain" == "https://"* ]]; then
        protocol="https://"
    fi
    case "$domain" in
    *"://"*) echo "${domain#*://}" ;;
    *"@"*) echo "${domain#*@}" ;;
    esac
}

git_url_path() {
    local repo_url="$1"
    local namespace=${repo_url#*.com/}
    echo $repo_url | sed 's#.*\.com[:/]\(.*\)\.git#\1#g'
}

git_api_repo() {
    local repo_url="$1"
    local domain=$(git_url_domain "$repo_url")
    local path=$(git_url_path "$repo_url")
    case "$domain" in
    *gitlab*) echo "https://$domain/api/v4/projects/${path//\//%2F}" ;;
    *github*) echo "https://api.github.com/repos/$path" ;;
    esac
}

download_res() {
    local res_url="$1"
    if [[ "$res_url" != "http://"* && "$res_url" != "https://"* ]]; then
        return 0
    fi
    local url_path="$(url_path "$res_url")"
    if [ -z "$url_path" ]; then
        echo "failed $res_url"
        return 0
    fi
    local output="$public_dir/-/$url_path"
    if [ -e "$output" ]; then
        echo "cahed $res_url"
        return 0
    fi
    mkdir -p $(dirname "$output")
    echo "download $res_url"
    curl -s "$res_url" >"$output"
}

url_path() {
    local url=$1
    if [[ "$url" == *"cdn.jsdelivr.net/npm/"* ]]; then
        echo "$url" | sed 's#.*cdn.jsdelivr.net/npm/\(.*\)#\1#g'
    else
        err "UNSUPPORTED URL: $url"
        return 1
    fi
}

json_get() {
    local key="$1"
    sed "s#.*\"$key\"\s*:\s*\"\([^\"]*\)\".*#\1#g"
}

conf_set() {
    local file="$1"
    local key="$2"
    local value="$3"
    if [ -z "$key" ]; then
        err conf_set $file: empty key
        return 1
    fi
    local match=$(sed -n "s#^$key=.*\$#\0#p" $file)
    if [ "$match" == "" ]; then
        echo $key=$value >>$file
    else
        sed -i'' -e "s#^$key=.*\$#$key=$value#g" $file
    fi
    # file-w2u $file
    echo $value
}

conf_get() {
    local file=$1
    local key=$2
    local fallback=$3
    if [ -z "$key" ]; then
        err ERROR: conf_get $file: empty key
        return 1
    fi
    local value=$(sed -n "s#^$key=\(.*\)\$#\1#p" $file)
    if [[ -z "$value" && -n "$fallback" ]]; then
        conf_set $file $key $fallback
        echo $fallback
    else
        echo $value
    fi
}

conf() {
    local file="$public_dir/main.conf"
    local key="$1"
    local value="$2"
    mkdir -p "$(dirname $file)"
    touch $file
    if [ -z "$value" ]; then
        conf_get "$file" "$key"
    else
        conf_set "$file" "$key" "$value"
    fi
}

err() {
    echo -e "\033[31m$@\033[0m" 1>&2
}

is_win() {
    case "$(uname -s)" in
    CYGWIN* | MINGW* | MSYS*) return 0 ;;
    *) return 1 ;;
    esac
}

win_batch() {
    MSYS_NO_PATHCONV=1 MSYS2_ARG_CONV_EXCL=* cmd /c "$@"
}

win_ln() {
    local FLAG_SOFT=0
    local FLAG_FORCE=0
    local FLAG_DIR=0
    while getopts ":sf" opt; do
        case "$opt" in
        s) FLAG_SOFT=1 ;;
        f) FLAG_FORCE=1 ;;
        esac
    done
    shift $(($OPTIND - 1))
    local src=$1
    local dest=$2
    local FLAGS=()
    if [ -d "$src" ]; then
        FLAGS+=('/j')
    elif [ "$FLAG_SOFT" = 0 ]; then
        FLAGS+=('/h')
    fi
    if [ "$FLAG_FORCE" = 1 -a -e "$dest" ]; then
        rm -rf "$dest"
    fi
    src=$(cygpath -w $(realpath $src))
    dest=$(cygpath -w $(realpath $dest))
    if [ "$src" = "$dest" ]; then
        err "Cannot self link for $src."
        return 1
    fi
    echo mklink "${FLAGS[@]}" "$dest" "$src"
    win_batch mklink "${FLAGS[@]}" "$dest" "$src"
}

ln_s() {
    local src_file="$1"
    local dest_file="$2"
    if [[ -e "$dest_file" ]]; then
        return 0
    fi
    if is_win; then
        win_ln -s "$src_file" "$dest_file"
    else
        ln -s "$src_file" "$dest_file"
    fi
}

main "$@"
